extends Area

var _life_time = 0.3
var _life_time_timer = 0.0

var speed = 24.0
var _damage = 30.0

func _process(delta):
    _run_life_time_timer(delta)

func _physics_process(delta):
    transform.origin += -transform.basis.z * speed * delta

func _run_life_time_timer(delta):
    if(_life_time_timer < _life_time):
        _life_time_timer += delta
    else:
        queue_free()

func get_damage():
    return _damage

func _on_Slash_body_entered(var body):
    if (body.is_in_group("enemy")):
        if (body.has_method("_on_collision_with_player_projectile")):
            body._on_collision_with_player_projectile(_damage)
            queue_free()