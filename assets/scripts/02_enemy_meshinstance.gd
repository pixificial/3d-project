extends MeshInstance

onready var enemy_02 = get_parent()

func _physics_process(delta):
   look_at(global_transform.origin + enemy_02.direction.normalized(), Vector3.UP)