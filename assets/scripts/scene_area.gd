extends Area

export(PackedScene) var scene_to_load

func _on_SceneArea_body_entered(var body):
	get_tree().change_scene_to(scene_to_load)
