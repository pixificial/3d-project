tool
extends Area

onready var destination_marker = get_node("DestinationMarker")

export(Vector3) var teleport_destination = transform.origin

func _process(delta):
    if(Engine.editor_hint):
        destination_marker.global_transform.origin = teleport_destination

func get_teleport_destination():
    return teleport_destination