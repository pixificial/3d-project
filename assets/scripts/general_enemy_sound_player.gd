extends AudioStreamPlayer

const EXISTENCE_DURATION = 1.5

enum Sounds {SOUND_HURT, SOUND_DEATH}

var existence_timer : float
var _enemy_hurt = preload("res://assets/sounds/enemy_hurt.ogg")
var _enemy_death = preload("res://assets/sounds/enemy_death.ogg")

func _ready():
    existence_timer = EXISTENCE_DURATION
    bus = "SFX"

func _process(delta):
    _run_existence_timer(delta)

func play_sound(var sound):
    match sound:
        Sounds.SOUND_HURT:
            stream = _enemy_hurt
        Sounds.SOUND_DEATH:
            stream = _enemy_death
        _:
            print("Error: Supposed to be Sounds enum.")
    play()

func _run_existence_timer(delta):
    if (existence_timer > 0.0):
        existence_timer -= delta
    else:
        queue_free()