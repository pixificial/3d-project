tool
extends Spatial

onready var text_box_viewport = get_node("TextBoxViewport")
onready var text_box_label = get_node("TextBoxViewport/TextBoxLabel")
onready var text_box_sprite_3d = get_node("TextBoxSprite3D")

export(String) var text
export(bool) var billboard

var viewport_texture : ViewportTexture

func _ready():
	text_box_label.set_text(text)
	text_box_viewport.update_size()
	text_box_sprite_3d.billboard = billboard

func _process(delta):
	if (Engine.editor_hint):
		text_box_viewport = get_node("TextBoxViewport")
		text_box_label = get_node("TextBoxViewport/TextBoxLabel")
		text_box_sprite_3d = get_node("TextBoxSprite3D")
		text_box_label.set_text(text)
		text_box_viewport.update_size()
		text_box_sprite_3d.billboard = billboard
