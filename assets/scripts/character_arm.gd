extends Spatial

onready var _animation_player = get_node("AnimationPlayer")
onready var _audio_stream_player = get_node("AudioStreamPlayer")

func play_animation():
    _animation_player.current_animation = "Cylinder002Action"
    _animation_player.play()
    _audio_stream_player.play()