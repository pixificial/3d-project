extends "res://assets/scripts/jumper.gd"

enum Enemy00State {STATE_IDLE, STATE_WANDER, STATE_CHASE, STATE_SEARCH}

const JUMP_COOLDOWN = 2.0
const SEARCH_COOLDOWN = 4.0

onready var generic_enemy_sound_player = preload("res://assets/scripts/general_enemy_sound_player.gd")

var state = Enemy00State.STATE_IDLE
var idle_wander_cycle_timer : float
var idle_wander_cycle_idle_duration = 4.0
var idle_wander_cycle_wander_duration = 2.0
var _initial_position : Vector3
var direction : Vector3
var max_wander_direction_randomness_distance = 20.0
var chased_body : PhysicsBody
var _jump_cooldown_timer = 0.0
var _horizontal_distance_to_chased_body : float
var _search_cooldown_timer = 0.0
var _damage = 20.0
var _health = 100.0

var colcount = 0

func _ready():
	_initial_position = global_transform.origin
	randomize()
	_set_inherited_variables()
	_start_idle_wander_cycle_timer_as_idle()

func _process(delta):
	_run_idle_wander_cycle_timer(delta)
	_run_jump_cooldown_timer(delta)
	_run_search_cooldown_timer(delta)

func _physics_process(delta):
	_wander(delta)
	_chase(delta)
	_search(delta)
	_collide_with_physicsbodys()

func _set_inherited_variables():
	_acceleration = 40.0
	_friction = 6.0
	_jump_strength = 12.0

func _run_idle_wander_cycle_timer(delta):
	if (idle_wander_cycle_timer > 0.0):
		idle_wander_cycle_timer -= delta
	else:
		match (state):
			Enemy00State.STATE_IDLE:
				_start_idle_wander_cycle_timer_as_wander()
				_set_state(Enemy00State.STATE_WANDER)
				_compute_direction_for_wander()
				#print("im now wandering")
			Enemy00State.STATE_WANDER:
				_start_idle_wander_cycle_timer_as_idle()
				_set_state(Enemy00State.STATE_IDLE)
				#print("im now idle")

func _start_idle_wander_cycle_timer_as_idle():
	idle_wander_cycle_timer = idle_wander_cycle_idle_duration + (randf() - 0.5) * 2.0

func _start_idle_wander_cycle_timer_as_wander():
	idle_wander_cycle_timer = idle_wander_cycle_wander_duration + (randf() - 0.5)

func _set_state(value):
	if (typeof(value) == TYPE_INT):
		state = value
		if(value == Enemy00State.STATE_SEARCH):
			_search_cooldown_timer = SEARCH_COOLDOWN
			direction = chased_body.global_transform.origin - global_transform.origin
			direction.y = 0
			direction = direction.normalized()

	else:
		print("Exception: Value is not an enum.")

func _wander(delta):
	if (state == Enemy00State.STATE_WANDER):
		_add_to_velocity(direction * _acceleration / 3.0 * delta)

func _compute_direction_for_wander():
	var direction_to_initial_position = _initial_position - global_transform.origin
	direction_to_initial_position.y = 0
	var distance_to_initial_position = direction_to_initial_position.length()
	direction_to_initial_position = direction_to_initial_position.normalized()
	var random_direction : Vector2
	random_direction.x = randf() - 0.5
	randomize()
	random_direction.y = randf() - 0.5
	random_direction = random_direction.normalized()
	direction = Vector3(random_direction.x * max((max_wander_direction_randomness_distance -
		distance_to_initial_position), 0.0) + direction_to_initial_position.x *
		min(max_wander_direction_randomness_distance, distance_to_initial_position), 0.0,
		random_direction.y * max((max_wander_direction_randomness_distance -
		distance_to_initial_position), 0.0) + direction_to_initial_position.z *
		min(max_wander_direction_randomness_distance,distance_to_initial_position)).normalized()

func _chase(delta):
	if (state == Enemy00State.STATE_CHASE):
		direction = (chased_body.global_transform.origin - global_transform.origin)
		direction.y = 0.0
		_horizontal_distance_to_chased_body = direction.length()
		direction = direction.normalized()
		if(is_on_floor()):
			_add_to_velocity(direction * _acceleration * delta)
			if (_jump_cooldown_timer <= 0.0):
				if (_horizontal_distance_to_chased_body < 3.0):
					if (chased_body.global_transform.origin.y > global_transform.origin.y + 1.0):
						_jump()
						_jump_cooldown_timer = JUMP_COOLDOWN
				elif (_velocity.length() < 3.0):
					_jump()
					_jump_cooldown_timer = JUMP_COOLDOWN
		else:
			_add_to_velocity(direction * _acceleration / 6.0 * delta)
		if(_horizontal_distance_to_chased_body > max_wander_direction_randomness_distance):
			_set_state(Enemy00State.STATE_SEARCH)

func _run_jump_cooldown_timer(delta):
	if (_jump_cooldown_timer > 0.0):
		_jump_cooldown_timer -= delta

func _on_PlayerDetector_body_entered(body):
	if(body.has_method("_move_with_movement_input")):
		_set_state(Enemy00State.STATE_CHASE)
		chased_body = body
		_jump_cooldown_timer = JUMP_COOLDOWN
		#print("im now chasing")

func _search(delta):
	if(state == Enemy00State.STATE_SEARCH):
		_add_to_velocity(direction * _acceleration * (_search_cooldown_timer / SEARCH_COOLDOWN) * delta)

func _run_search_cooldown_timer(delta):
	if (_search_cooldown_timer > 0.0):
		_search_cooldown_timer -= delta
	elif(state == Enemy00State.STATE_SEARCH):
		_set_state(Enemy00State.STATE_WANDER)
		
func _collide_with_physicsbodys():
	var colliding_object : Object
	if(get_slide_count() > 0):
		for i in get_slide_count():
			colliding_object = get_slide_collision(i).collider
			if (colliding_object.is_in_group("player") && colliding_object.has_method("_on_collision_with_enemies")):
				colliding_object._on_collision_with_enemies(-get_slide_collision(i).normal, _damage)
				colcount += 1
				#print("enemy colcount: " + str(colcount)) 

func _set_health(var value):
	_health = value

func _on_collision_with_player_projectile(var damage):
	if (typeof(damage) == TYPE_REAL):
		_set_health(_health - damage)
		play_generic_enemy_sound(generic_enemy_sound_player.Sounds.SOUND_HURT)
	if (_health <= 0.0):
		play_generic_enemy_sound(generic_enemy_sound_player.Sounds.SOUND_DEATH)
		queue_free()

func get_damage():
	return _damage

func get_health():
	return _health

func play_generic_enemy_sound(var generic_enemy_sound):
	var generic_hurt_sound = generic_enemy_sound_player.new()
	get_tree().get_current_scene().add_child(generic_hurt_sound)
	generic_hurt_sound.play_sound(generic_enemy_sound)
