extends AudioStreamPlayer

onready var ambience = preload("res://assets/sounds/ambience.ogg")

func _ready():
	bus = "Music"
	stream = ambience
	autoplay = true
	play()
	
